#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

from __future__ import print_function

import pika
import json
import sys
import argparse
from lc_ldap.shortcuts import lc_ldap_admin
from affich_tools import prompt
import lc_ldap.filter2 as filter

from client import Ticket

ldap = lc_ldap_admin()

conf_wifi_only = True
conf_reset_password = False
conf_filter = None


parser = argparse.ArgumentParser()
parser.add_argument("--all", help="Affiche toutes les machines, fixe et mobiles", action="store_true")
parser.add_argument("--pwd", '--pass', help="Réinitialise le mot de passe de l'adhérent", action="store_true")
parser.add_argument("--debug", help="Réinitialise le mot de passe de l'adhérent", action="store_true")
parser.add_argument("--aux", help="Choisir l'imprimante auxilliaire", action="store_true")
parser.add_argument("--forced", help="Ne demande pas de confirmation", action="store_true")
parser.add_argument("conf_filter", help="Filtre pour la recherche d'un adhérent dans la bas")
args = parser.parse_args()


if args.all:
    conf_wifi_only = False
elif args.pwd:
    conf_reset_password = True
elif args.debug:
    pass
elif args.aux:
    pass

conf_filter = args.conf_filter

f = filter.human_to_ldap(conf_filter.decode('utf-8'))
res = ldap.search(f, mode='rw')
if len(res) > 1:
    print("More than one result")
    exit(1)
elif not res:
    print("Nobody found")
    exit(2)
else:
    item = res[0]
    if 'uid' not in item:
        conf_reset_password = False

    item.display()
    if conf_reset_password:
        print("Le mot de passe (compte Crans) sera également réinitialisé")

    if not args.forced:
        while True:
            c = prompt("[O/N]").lower()
            if c == 'n':
                exit()
            elif c == 'o':
                break

ticket = Ticket(args.debug,args.aux)
if 'uid' in item and conf_reset_password:
    ticket.reset_password(item)

if item.ldap_name is 'facture':
    ticket.add_facture(item)
elif hasattr(item, 'machines'):
    for m in item.machines():
        if not conf_wifi_only or 'machineWifi' in m['objectClass']:
            ticket.add_machine(m)
else:
    ticket.add_machine(item)

ticket.print()

