#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
"""Script permettant de réinitialiser un mot de passe de compte
LDAP, et d'imprimer un ticket avec les informations pertinentes"""

from __future__ import print_function

import sys
import argparse

from lc_ldap import crans_utils
from lc_ldap import shortcuts
from cransticket.client import Ticket

def reset_pass(args, ldap):
    """Retourne l'adhérent cherché"""
    _result = ldap.search(u'uid=%s' % (crans_utils.escape(args.login),), mode="w")

    if not _result:
        print("Adhérent introuvable")
        sys.exit(1)

    ticket = Ticket()
    ticket.reset_password(_result[0])
    if args.test:
        print("Mot de passe changé (on est en test, donc pas de ticket)")
    else:
        ticket.print()

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description="Réinitialisation d'un mot de passe d'un compte.", add_help=False)
    PARSER.add_argument('-h', '--help', help="Affiche ce message et quitte.", action="store_true")
    PARSER.add_argument('--test', help="Se connecter à la base de test", action="store_true")
    PARSER.add_argument('login', type=str, nargs="?", help="Le login du compte à modifier")

    ARGS = PARSER.parse_args()

    if ARGS.help:
        PARSER.print_help()
        sys.exit(0)
    if ARGS.test:
        LDAP = shortcuts.lc_ldap_test()
    else:
        LDAP = shortcuts.lc_ldap_admin()

    reset_pass(ARGS, LDAP)
