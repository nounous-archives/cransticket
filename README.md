# Crans Ticket

## Matériel
 * Une raspberry pi modèle B
 * Une carte SD
 * Alim 5V (beaucoup d'A ?)
 * Prise femelle (pour cette alim)
 * Câble micro-usb
 * [Batterie de secours](http://www.priceminister.com/offer/buy/364740224/urbanrevolt-19857.html) (24€)
 * [Imprimante thermique](http://www.adafruit.com/products/597) (~50€)
 * Boîte de ferrero

## Installation du dépôt sur l'imprimante

Paquets nécessaires à cransticket :

    #rajouter wheezy-backports dans sources.list
    apt-get update
    apt-get -t wheezy-backports python-pika
    apt-get install python-imaging

Pour le dépôt lui-même :

    git clone $le_dépot
    cd $le_dépot
    git submodule init
    git submodule update

On peut tester l'imprimante seule en lançant dump.py.

## Autorisations sur rabbitmq

Ci dessous, le serveur rabbitmq s'appelle "civet" et le client "cransticket"
se nomme "rasputin".

sur civet:
    sudo rabbitmqctl add_user rasputin $mdp
    sudo rabbitmqctl set_permissions rasputin "(amq\.default|CransTicket)" "(amq\.default|CransTicket)" "(amq\.default|CransTicket)"
  
## TODO (à mettre en forme dans la doc)
    sudo rabbitmqctl list_permissionsr
    sudo rabbitmq list_permissions
    sudo rabbitmqctl list_queues
    sudo rabbitmqctl help | less
    sudo rabbitmqctl trace 
    sudo rabbitmqctl trace_on
    sudo less /var/log/rabbitmq/rabbit@civet.log
    sudo rabbitmqctl list_permissions
    sudo rabbitmqctl set_permissions oie ".*" ".*" ".*"
    sudo rabbitmqctl set_permissions oie "CransTicket" "CransTicket" "CransTicket"
    sudo rabbitmqctl set_permissions oie "CransTicket" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "amq\.default" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "amq\.default|CransTicket" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "amq\.default\|CransTicket" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "(amq\.default|CransTicket)" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "\(amq\.default|CransTicket\)" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "(amq\.default|CransTicket)" "CransTicket" "(amq\.default|CransTicket)"

## Démarrer automatiquement le service CransTicket lors du boot de la machine


Pour que les tickets puissent être imprimés sans avoir besoin de lancer manuellement le service sur la raspberry, il faut :
### Avec l'initscript
 * Placer l'initscript de cransticket dans `/etc/init.d/`
 * Entrer la commande suivante
    # update-rc.d cransticket defaults

### Avec systemd
 * créer un user cransticket sans mdp:
    # adduser --disabled-login --disabled-password cransticket
 * ajouter cransticke au group dialout:
    #usermod -a -G dialout cransticket
 * cloner le dépot dans le home de cransticket
 * copier cransticket.service dans /etc/systemd/system/:
    # cp /home/cransticket/cransticket/cransticket.service /etc/systemd/system/
 * activer l'unit file dans systemd:
    # systemctl daemon-reload
    # systemctl enable cransticket.service

# Chiffrer la carte de la ticketeuse

Pour protéger les données stockées sur la carte des ticketeuses, on chiffre sa partition racine.
Lorsque la raspberry boote, il est nécessaire de rentrer une passphrase pour déchiffrer le disque afin
que la machine puisse finir de charger le système d'exploitation.

Cette passphrase peut-être rentrée au clavier sur la raspberry ou en se connectant avec une clé SSH.

Ce qui suit est un résumé de la procédure décrite sur [cette page](paxswill.com/blog/2013/11/04/encrypted-raspberry-pi/)

## Paquets nécessaires
 * `busybox`
 * `cryptsetup`
 * `dropbear` (Pour le déverouillage à distance uniquement)

Il faut donc commencer par les installer

    # apt-get install busybox cryptsetup

## Génération d'une première initramfs

On commence par générer une première initramfs

    # mkinitramfs -o /boot/initramfs.gz

Puis on demande à la raspberry de booter dessus au prochain démarrage en ajoutant à la fin du fichier `/boot/config.txt`
la ligne suivante :

    initramfs initramfs.gz followkernel

## Chiffrement du slash de la ticketeuse

À partir d'ici, il faut une autre machine sous Linux capable de monter la carte SD de la raspberry
Il faut repérer lorsque la carte est insérée à quel périphérique elle correspond, par exemple `/dev/sdb` (cela peut très bien être `/dev/mmcblk0p` ou autre chose). On peut utiliser le dmesg pour ça.

On commence par sauvegarder sur la deuxième machine les données de la ticketeuse de façon à pouvoir chiffrer le / (ce qui va formatter la partition et donc effacer ses données) avant de les y remettre.

    # dd if=/dev/sdb2 of=/tmp/raspbian-plain.img bs=4M
    # e2fsck -f /tmp/raspbian-plain.img
    # resize2fs -M /tmp/raspbian-plain.img

Toujours sur l'ordinateur (*pas sur la rPi*), il faut installer `cryptsetup`

    # apt-get install cryptsetup

On chiffre ensuite la partition avant de la déverrouiller

    # cryptsetup -v -y --cipher aes-cbc-essiv:sha256 --key-size 256 luksFormat /dev/sdb2
    # cryptsetup -v luksOpen /dev/sdb2 sdcard

Puis on restaure les données

    # dd if=/tmp/raspbian-plain.img of=/dev/mapper/sdcard bs=4M
    # e2fsck /dev/mapper/sdcard
    # resize2fs /dev/mapper/sdcard

Il reste à expliquer à la raspberry comment booter sur ces nouvelles partitions

    # mkdir /tmp/pi_root /tmp/pi_boot
    # mount /dev/sdb1 /tmp/pi_boot
    # mount /dev/mapper/sdcard /tmp/pi_root

Le fichier `/tmp/pi_boot/cmdline.txt` doit être modifié : il faut remplacer `root=/dev/mmcblk0p2` par `root=/dev/mapper/sdcard cryptdevice=/dev/mmcblk0p2:sdcard`.
Il faut ensuite adapter le `/tmp/pi_root/etc/fstab` pour que la rPi boote correctement : `/dev/mmcblk0p2` doit être changé en `/dev/mapper/sdcard`
Enfin, un nouveau fichier `/tmp/pi_root/etc/crypttab` doit être créé. Il doit contenir :

    sdcard  /dev/mmcblk0p2  none    luks

Tous les systèmesde fichiers peuvent alors être démontés et la carte replacée dans la rPi

    # umount /tmp/pi_boot/ /tmp/pi_root/
    # cryptsetup luksClose sdcard

## Démarrage

Il faut alors connecter un écran et un clavier à la raspberry Pi, car **le premier boot va rater**, car la racine du système ne sera pas accessible avant que la partition ne soit déchiffrée.
Lorsque le système ouvre un shell de secours (lignes commençant par `(initramfs)`), les commandes suivantes permettent de déchiffrer la carte pour que la rPi puisse continuer la séquence de boot

    (initramfs) cryptsetup luksOpen /dev/mmcblk0p2 sdcard
    (initramfs) exit

Une fois le boot terminé, on régénère l'initramfs

    # mkinitramfs -o /boot/initramfs.gz

La machine peut être redémarrée sans crainte, la passphrase sera gentillement demandée lors du boot

## Déchiffrement à distance

Pour déchiffer la partition contenant la racine du système à distance, on installe dropbear, un serveur SSH léger qui permettra de se connecter avant le montage de la racine avec un clé SSH.

    # apt-get install dropbear

Le code de dropbear dans Raspbian contient une erreur qu'il faut corriger : À la ligne 296 du fichier `/usr/share/initramfs-tools/scripts/local-top/cryptroot`, il faut ajouter `/sbin/` avant `blkid`, puis régénérer l'initramfs

    # mkinitramfs -o /boot/initramfs.gz

Copier la clé SSH générée par dropbear (`/etc/initramfs-tools/root/.ssh/id_rsa`) sur un autre ordinateur, puis l'ajouter au fichier `/etc/initramfs-tools/root/.ssh/authorized_keys`.
Ajouter juste avant le `ssh-rsa` l'instruction suivante :

    command="/scripts/local-top/cryptroot && kill -9 `ps | grep -m 1 'cryptroot' | cut -d ' ' -f 3`"

Il reste à régénérer l'initramfs une dernière fois

    # mkinitramfs -o /boot/initramfs.gz

Il ne reste plus qu'à tester en redémarrant la rPi et en essayant de s'y connecter pour taper la passphrase

    ssh -i chemin/vers/la/clé root@<nom de la ticketeuse>
