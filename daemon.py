#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys,os,pwd,grp
# Bad namming: change namming
import common
import pika
import json
import dump
import config
import threading, time, smtplib
import argparse
from email.mime.text import MIMEText
from config import DEVICE
import codecs

def run():
    conn = pika.BlockingConnection(config.PARAMS)
    ch = conn.channel()
    ch.queue_declare(queue=config.QUEUE)
    def callback(ch, method, properties, body):
        print (" [x] Received %r" % (body,))
        dump.print_liste(json.loads(body.decode('utf8')))
    
    ch.basic_consume(callback, queue=config.QUEUE, no_ack=True)
    ch.start_consuming()
    conn.close()
# fork en arrière plan + pidfile

def checkPaper():
    printer = AdafruitThermal(DEVICE, 19200, timeout=5)
    messageCourant =  False
    messagePapier = False

    def sendMel(msgfile, etat):
        fp = open(msgfile, 'rb')
        msg = MIMEText(fp.read())
        fp.close()

        me = 'oison@crans.org'
        dest = 'respbats@crans.org'
        msg['Subject'] = '''Etat de l'imprimante thermique : %s''' % etat
        msg['From'] = me
        msg['To'] = dest

        s = smtplib.SMTP('smtp.crans.org', 25)
        s.sendmail(me, [dest], msg.as_string())
        s.quit()

    def checkPrinter(dejaVu, messageCourant, messagePapier):
        try:
            papier = printer.hasPaper()
            if messageCourant:
                sendMel('melSecteur', 'alimentation')
                messageCourant = False
            else:
                if not (messagePapier or papier):
                    sendMel('melPasPapier', 'papier')
                    messagePapier = True
                if messagePapier and papier:
                    sendMel('melPapier', 'papier')
                    messagePapier = False
        except TypeError:
            if not messageCourant:
                if dejaVu:
                    sendMel('melSurBatterie', 'alimentation')
                    messageCourant = True
                else:
                    time.sleep(600)
                    messageCourant, messagePapier = checkPrinter(True, messageCourant, messagePapier)
        return messageCourant, messagePapier

    while True:
        time.sleep(60)
        messageCourant, messagePapier = checkPrinter(False, messageCourant, messagePapier)
    
if __name__ == "__main__":
    # Parse argument with argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-fg", help="run the daemon in forground mode", action="store_true")
    args = parser.parse_args()

    # do the UNIX double-fork magic, see Stevens' "Advanced
    # Programming in the UNIX Environment" for details (ISBN 0201563177)
    try:
        pid = os.fork()
        if pid > 0:
            # exit first parent
            sys.exit(0)
    except OSError as e:
        print("fork #1 failed: %d (%s)" % (e.errno, e.strerror),file=sys.stderr)
        sys.exit(1)

    # decouple from parent environment
    os.chdir("/")   #don't prevent unmounting....
    os.setsid()
    os.umask(0)

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # exit from second parent, print eventual PID before
            print("Daemon PID %d" % pid)
            open(common.PIDFILE, 'w').write("%d" % pid)
            sys.exit(0)
    except OSError as e:
        print("fork #2 failed: %d (%s)" % (e.errno, e.strerror),file=sys.stderr)
        sys.exit(1)

    # start the daemon thread to monitor the printer
    #thHasPaper = threading.Thread(target = checkPaper, name = 'checkPaper')
    #thHasPaper.start()
    #main loop
    run()

