#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
# Bad namming: change namming
from escpos.printer import Usb
import subprocess, time, socket
import os
import json
import config
#

SAMPLE_MACHINE = {
  'host': 'nouille',
  'macAddress': '<automatique>',
  'secret': '**********',
}

SAMPLE_ACCOUNT = {
  'login': 'passoir',
  'pass': 'omgverysecure',
}



""" Seiko Epson Corp. Receipt Printer (EPSON TM-T88III) """
printer = Usb(config.VENDOR_ID, config.DEVICE_ID, 0)
printer.charcode(code='CA_FRENCH')

def print_carac(text, value):
    pad = 384/12 - len(text) - len(value)
    printer.text(text + ('.'*int(pad)) + value + '\n')


def show_entry(entry):
    # Si le champ fid est présent, c'est une facture, on utilise une fonction
    # annexe
    if 'fid' in entry.keys():
        show_facture(entry)
        return
    
    printer.text("\n")
    printer.set(align='center', text_type="BU")
    if 'host' in entry:
        title = 'Détails machine'
        if 'type' in entry:
            title += ' ' + entry['type']
        printer.text(title + '\n')
    else:
        printer.text('Compte Crans \n')
    printer.set(align='center', text_type="NORMAL")

    login = entry.get('login', None) or entry['host']
    print_carac('Login', login)
    if 'macAddress' in entry:
        print_carac('Mac', entry['macAddress'])
    if 'secret' in entry:
        print_carac('Mot de passe', entry['secret'])
    if 'pass' in entry:
        print_carac('Mot de passe', entry['pass'])
        printer.text('\n')
        printer.set(align='center', text_type="BU")
        printer.text('Connectez-vous sur https://intranet.crans.org\n')
        printer.text('pour gérer votre compte et vos machines\n') 
        printer.set(align='center', text_type="NORMAL")


def make_fact_elem(element,nb_car,is_last):

    ligne = u''

    u_elem = str(element)
    if len(u_elem) >= nb_car:
        ligne += u_elem[0:nb_car]
    else:
        if is_last:
            for i in range(nb_car-len(u_elem)):
                ligne += u' '
            ligne += u_elem
        else:
            ligne += u_elem
            for i in range(nb_car-len(u_elem)):
                ligne += u' '

    if is_last:
        ligne += u''
    else:
        ligne += u'|'

    return ligne

# Fonction spéciale pour l'impression des factures
# ! facture est un dictionnaire, pas un objet LDAP !
def show_facture(facture):

    printer.text('\n')
    #Impression de l'en-tête
    printer.text(facture['recuPaiement'] + '\n')
    printer.text('--------------------------------\n')
    printer.set(align='center', text_type="BU")
    fid = u'Facture n°%d' % facture['fid']
    printer.text(fid + '\n')
    printer.set(align='center', text_type="NORMAL")
    printer.text('--------------------------------\n')

    #Impression de l'en-tête de la facture
    nom_complet = facture['nom'] + u' ' + facture['prenom']
    printer.text('Adhérent : ' + nom_complet + '\n')
    if facture['chbre'] is u'EXT':
        printer.text('Externe \n')
    else:
        printer.text(('Chambre : ' + facture['chbre'] + '\n'))
    if facture['debutAdhesion']:
        printer.text(('Début adhésion : ' + facture['debutAdhesion'] + '\n'))
    if facture['finAdhesion']:
       printer.text(('Fin adhésion : ' + facture['finAdhesion']+ '\n'))
    if facture['finConnexion']:   
       printer.text(('Fin connexion : ' + facture['finConnexion']+ '\n'))

    #printer.justify('L')
    printer.text('--------------------------------\n')
    printer.text(' Code |   Désig.  |Qté|   P.U   \n') #Code(6c. max.),Désignation(11c. max.),Qté(3c. max),PU(8[+1]c.max)
    printer.text('------|-----------|---|---------\n')

    #Impression du corps de la facture + calcul du prix total
    total = 0.0
    for art in facture['article']:
        row = u''
        row += make_fact_elem(art['code'],6,False)
        row += make_fact_elem(art['designation'],11,False)
        row += make_fact_elem(art['nombre'],3,False)
        row += make_fact_elem(art['pu']+u'e',9,True)
        total += float(art['pu'])

        printer.text((row + '\n'))

    printer.text(u'--------------------------------\n')

    #On affiche le total
    printer.text(('---------\n'))
    printer.text(('Total à régler  '+ '|' + make_fact_elem(total,8,True) + u'e' + '\n'))
    printer.text(('---------\n'))
    printer.text(('Payé par ' + facture['modePaiement'] + '\n'))

    #On met une super phrase sur la TVA
    printer.text('\n')
    printer.set(align='center', text_type="BU")
    printer.text('TVA non applicable(art. 293B du CGI)\n')
    printer.set(align='center', text_type="NORMALE")
    printer.text('\n')
    printer.text(config.ADDRESS)


# Do print
def print_liste(liste):
    printer.set(align='center')
    printer.image(os.path.dirname(os.path.realpath(sys.argv[0])) + "/logo_crans.png", True)
    
    for entry in liste:
        show_entry(entry)
    
    if any('secret' in entry or 'pass' in entry for entry in liste):
        printer.text(' \n')
        printer.text(' \n')
        printer.text(str('Veuillez conserver ces \n'))
        printer.text(str('informations en lieu sûr. \n'))
    
    printer.cut()

if __name__ == '__main__':
    if not sys.argv[1:]:
        liste = [SAMPLE_ACCOUNT, SAMPLE_MACHINE]
    else:
        with open(sys.argv[1], 'r') as f:
            liste = json.load(f)
    print_liste(liste)

